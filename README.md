# Codementor: Creating a Simple Application (Using React JS and Flux Architecture)
This is an implementation of a codementor tutorial, the tutorial is freely avaliable at: https://www.codementor.io/reactjs/tutorial/react-js-flux-architecture-tutorial, the article describes the basics of creating an app using flux with reactjs, this code is the final product of said tutorial

## Pre-requisites
To test this project you'll need
- [git](https://git-scm.com/)

## Installing

Clone this repository and install the app by typing this on a terminal

```
git clone https://ygorthemoster@gitlab.com/ygorthemoster/React_Flux_Codemasters.git
cd React_Flux_Codemasters/app
npm install
```

## Running
run using:
```
npm start
```

## License
See [LICENSE](LICENSE)

## Acknowledgments
- Original source by [Chris Harrington](https://github.com/chrisharrington) available at: https://github.com/chrisharrington/react-todo
- Tutorial by [Codementor Chris Harrington](https://www.codementor.io/chrisharrington) available at: https://www.codementor.io/reactjs/tutorial/react-js-flux-architecture-tutorial
